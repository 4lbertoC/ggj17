(function() {

  var elementsToMove = jQuery('a, img, .field__label, .field__item, .view-footer, .site-slogan, .copyright, .logo, h1, h2, h3, .user');
  var $win = jQuery(window);
  var winWidth = $win.width();
  var winHeight = $win.height();

  function prepareElements(etm) {
      etm.css({
         position: 'fixed',
         transition: 'left 1000ms',
         top: 0,
         "font-family": "Comic Sans MS"
      });
  }

  function shuffleElements(etm) {
      etm.each(function(index, el) {
          jQuery(el).css({
              top: (Math.random() * winHeight) + 'px'
          })
      });
  }

  function moveElements(etm) {
      etm.each(function(index, el) {
         jQuery(el).css({
             left: (Math.random() * winWidth) + 'px'
         })
      });
  }

  function runBlueBg() {
      var bg = jQuery('<div class="PLL">');
      bg.css({
          position: 'fixed',
          width: '100%',
          height: '100%',
          top: 0,
          left: 0,
          background: 'blue',
          "font-family": "Comic Sans MS"
      });

      for(var i = 0; i < 300; i++) {
          var text = jQuery('<span>');
          text.text('WAVES?');
          text.css({
              position: 'absolute',
              top: (Math.random() * winHeight) + 'px',
              left: (Math.random() * winWidth - 100) + 'px',
              color: 'yellow',
              'font-size': Math.round(10 + Math.random() * 20) + 'px'
          });
          bg.append(text);
      }

      var showBlueBg = function() {
          jQuery('body').append(bg);
          var flickerDelay = Math.round(100 + Math.random() * 200);
          setTimeout(function() {
              var repetitionDelay = Math.round(Math.random() * 2000);
              setTimeout(showBlueBg, repetitionDelay);
              bg.find('span').each(function(index, el) {
                  var text = jQuery(el);
                  if (Math.random() > 0.8) {
                     text.text('WAVES?');
                     text.css({
                         'font-size': '30px'
                     });
                  } else {
                      text.text(Math.round(1e30 * Math.random()).toString(36));
                     text.css({
                         'font-size': Math.round(10 + Math.random() * 10) + 'px'
                     });
                  }
              });
              shuffleElements(elementsToMove);
              bg.remove();
          }, flickerDelay);
      };
      showBlueBg();
  }

  function showPll() {
    var style = jQuery('<link rel="stylesheet" type="text/css" href="http://207.154.201.20/waves/style/main.css">');
    jQuery('head').append(style);

    var pll = jQuery('<div class="ppl"><div class="ppl_arm"></div></div>');
    jQuery('body').append(pll);
  }

  prepareElements(elementsToMove);
  shuffleElements(elementsToMove);
  moveElements(elementsToMove);
  runBlueBg();
  jQuery(document).one('keyup', function() {
   showPll();
  });
  setInterval(function() {
      moveElements(elementsToMove);
  }, 500)
  setInterval(function() {
      moveElements(elementsToMove);
  }, 600);
  setInterval(function() {
      moveElements(elementsToMove);
  }, 700);

})();
