function writeText(textContent, x, y, dissolveAfter, additionalCss, onStart, onEnd, endTime) {
	onStart && onStart();
	var text = $('<div class="content-text">' + textContent + '</div>');
	if (x && y) {
		text.css({
			position: 'absolute',
			left: x + '%',
			top: y + '%'
		});
	}
	if (additionalCss) {
		text.css(additionalCss);
	}
	if (dissolveAfter) {
		setTimeout(function() {
			text.css({
				opacity: 0
			});
		}, dissolveAfter);
	}
	if (endTime && onEnd) {
		setTimeout(onEnd, endTime);
	}
	$('.content').append(text);
	return text;
}

function highlightInitials(container) {
	var words = container.text().split(' ');
	var newText = words.map(function(word) {
		return '<span class="waves-word">' + word.split('')[0] + '</span>' + '<span class="non-waves-word">' + word.substr(1) + '</span>';
	}).join(' ');
	container.html(newText);
	setTimeout(function() {
		container.remove();
	}, 3000);
}

function showWavesSentence(args) {
	var text = writeText.apply(this, arguments);
			setTimeout(function() {
				highlightInitials(text);
			}, 4000);
}

function showSentence(args) {
	var text = writeText.apply(this, arguments);
	setTimeout(function() {
		text.css({opacity: 0});
	}, 4000);
}

function getGuthrie() {
	$('.guthrie').remove();
	var guthrie = $('<div class="guthrie">ຄື້ນ</div>');
	$('.content').append(guthrie);
	setTimeout(function() {
		guthrie.css({opacity: 1});
	}, 0);
	return guthrie;
}

function guthrieSays(textContent, x, y) {
	var text = $('<div class="guthrie-voice">');
	text.text(textContent);
	if (x && y) {
		text.css({
			top: y + 'px',
			left: x + 'px'
		});
	}
	$('.guthrie').append(text);
	setTimeout(function() {
		text.remove();
	}, 2000);
}

function step1() {
	$('.loading').remove();
	if (window.localStorage['guthrieIsDead']) {
		var tomb = $('<div class="tomb">†</div>');
		tomb.click(function() {
			window.localStorage.clear();
			window.location.reload();
		})
		$('.content').append(tomb);
		return;
	}

	var guthrie = getGuthrie();
	if (window.localStorage['askedFood']) {
		var food = $('<div class="food">');
		food.text('⠺⠁⠧⠑⠎');
		food.css({
			position: 'absolute',
			top: '85%',
			left: '15%',
			fontSize: '15px'
		});

		var eatFood = function(event) {
			$(event.target).remove();
			guthrie.find('.guthrie-voice').remove();
			guthrieSays('stop eating my food!', 60, 30);
		}

		$('.content').append(food.click(eatFood));
		$('.content').append(food.clone().click(eatFood).css({top: '87%', left: '17%'}));
		$('.content').append(food.clone().click(eatFood).css({top: '83%', left: '19%'}));
		setTimeout(function() {
			guthrieSays('Oh nice, food!');
			setTimeout(function() {
				guthrie.addClass('guthrie-hiding');
				guthrie.css({
					width: 0
				});
				setTimeout(step3, 1000);
			}, 1000);
		}, 1000);
	} else {
		guthrie.one('mouseover', function(event) {
			guthrie.addClass('guthrie-hiding');
			guthrie.css({
				width: 0
			});
			writeText('Guthrie', 40, 35, 2000, {fontSize: '30px'});
			setTimeout(function() {
				var continueTimeoutHandle, secondaryTextTimeoutHandle;
				var registerSpace = function() {
					$(document).keydown(function(evt) {
						if (evt.which === 32) {
							unregisterSpace();
							clearTimeout(continueTimeoutHandle);
							clearTimeout(secondaryTextTimeoutHandle);
							$('.content').empty();
							step4();
						}
					});
				};
				var unregisterSpace = function() {
					$(document).off('keydown');
				};

				if (window.localStorage['foodEaten']) {
					showWavesSentence('was a very enjoyable slug...', 50, 60, null, null, registerSpace, unregisterSpace, 6000);
					secondaryTextTimeoutHandle = setTimeout(function() {
						showSentence('...and a great friend of yours.', 20, 80);
					}, 3000);
				} else {
					showWavesSentence('was a very elusive slug.', 50, 60, null, null, registerSpace, unregisterSpace, 6000);
				}
				
				continueTimeoutHandle = setTimeout(function() {
					guthrie.remove();
					step2();
				}, window.localStorage['foodEaten'] ? 6000 : 3000);
			}, 500);
		});
	}
}

function step2() {
	var guthrie = null;
	var followMouse = function(event) {
		if (!guthrie) {
			guthrie = getGuthrie();
			if (window.localStorage['foodEaten']) {
				showSentence('He would never invade your personal space, would he?', 40, 20);
				setTimeout(function() {
					showSentence('Especially while you are contemplating the restless seashore.', 20, 80);
				}, 3000);
			} else {
				showSentence('He would not deny company though.', 20, 80);
			}
			
			setTimeout(function() {
				window.localStorage['askedFood'] = 'true';

				guthrieSays('hello!', 50, -20);
				setTimeout(function() { guthrieSays('how are you?', -10, 60); }, 500);
				setTimeout(function() { guthrieSays('I like your face!', -30, -30); }, 2500);
				setTimeout(function() { guthrieSays('are you also hungry?', 25, 60); }, 3000);
				setTimeout(function() { guthrieSays('because I am hungry', 25, -20); }, 4500);
				setTimeout(function() { guthrieSays('and there is not much to eat here', -40, 60); }, 5000);
				setTimeout(function() { guthrieSays('can you bring me some food?', -30, -30); }, 6500);
				setTimeout(function() { guthrieSays('possibly something refreshing', -20, 60); }, 10000);
				setTimeout(function() { guthrieSays('can you bring me something refreshing?', 20, -20); }, 14000);

				if (window.localStorage['foodEaten']) {
					setTimeout(function() {	showSentence('And you would never deny his company...', 10, 10); }, 4500);
					setTimeout(function() {	showSentence('...would you?', 60, 80); }, 7000);
				} else {
					setTimeout(function() {	showSentence('Maybe he was a bit annoying...', 10, 10); }, 4500);
					setTimeout(function() {	showSentence('...or simply lonely?', 60, 80); }, 6500);
				}

				setTimeout(function() {
					guthrieSays('please, bring me something refreshing!', 20, -20);
					if (window.localStorage['foodEaten']) {
						showSentence('Of course you would not.');
						setTimeout(function() { showSentence('Now ease the pressure of your space...', 10, 10); }, 2000);
						setTimeout(function() { showSentence('...while you watch that beautiful, restless seashore.', 20, 80); }, 4000);
					} else {
						showSentence('Definitely annoying.');
					}
					setInterval(function() {
						guthrieSays('please, bring me something refreshing!', 20, -20);
					}, 3000);
				}, 18000);
			}, 2000);
		}
		guthrie.css({
			position: 'fixed',
			left: event.clientX + 'px',
			top: event.clientY + 'px'
		});
	}
	$(document).on('mousemove', function(event) {
		window.requestAnimationFrame(function() {
			followMouse(event);
		});
	});
}

function step3() {
	var guthrie = getGuthrie();
	guthrie.css({
		position: 'absolute',
		top: '80%',
		left: '22%'
	});
	var step3Interval = setInterval(function() {
		if ($('.food').length > 0) {
			guthrieSays('nom nom nom...', 10, -20);		
		} else {
			clearInterval(step3Interval);
			window.localStorage.removeItem('askedFood');
			window.localStorage['foodEaten'] = 'true';
			$(document).on('mousemove', function(event) {
				window.requestAnimationFrame(function() {
					guthrie.css({
						position: 'fixed',
						left: event.clientX + 'px',
						top: event.clientY + 'px'
					});
				});
			});
			guthrie.addClass('guthrie-yell');
			guthrieSays('why did you eat all my food?', 10, -30);
			showSentence('Did I tell you he was annoying?');
			setInterval(function() {
				guthrieSays('why did you eat all my food?', 10, -30);
			}, 3000);
			setTimeout(function() {
				$('body').addClass('black');
				guthrie.addClass('guthrie-hidden');
				showSentence('Let\'s maybe... leave him alone for a moment.', 30, 30);
				setTimeout(function() {
					window.location.reload();
				}, 5000);
			}, 5000);
		}
	}, 4000);
}

function step4() {
	var body = $('body');
	var win = $(window);
	var winWidth = win.width();
	var winHeight = win.height();
	var guthrie = getGuthrie();
	var timeoutHandles = [];
	guthrie.addClass('guthrie-closeup');
	guthrie.mousedown(function(event) {
		guthrie.off('mousedown');
		guthrie.find('.guthrie-voice').remove();
		guthrieSays('thanks buddy!', 20, -20);
		guthrie.removeClass('guthrie-closeup');
		guthrie.css({
			position: 'fixed',
			left: event.clientX + 'px',
			top: event.clientY + 'px'
		});
		timeoutHandles.forEach(function(handle) {
			clearTimeout(handle);
		});
		$(document).on('mousemove', function(event) {
			window.requestAnimationFrame(function() {
				guthrie.css({
					position: 'fixed',
					left: event.clientX + 'px',
					top: event.clientY + 'px'
				});
			});
		});
		step5();
	});
	guthrieSays('Please, take me with you!', -150, -70);
	timeoutHandles.push(setTimeout(function() { guthrieSays('I have a bad feeling.', -180, -70); }, 4000));
	timeoutHandles.push(setTimeout(function() {
		window.localStorage['guthrieIsDead'] = true;
		guthrie.find('.guthrie-voice').remove();
		guthrieSays('AAARGH!!!', -150, -70);
		guthrie.off('click');
		window.onbeforeunload = function() { return ':('; };

		for(var i = 0; i < 200; i++) {
			var blood = $('<div class="blood">');
			var radius = (5 + Math.random() * 100);
			blood.css({
				position: 'fixed',
				width: radius + 'px',
				height: radius + 'px',
				top: Math.round(Math.random() * winHeight) + 'px',
				left: Math.round(Math.random() * winWidth) + 'px',
				'border-radius': '100px',
				'background-color': 'rgba(0, 255, 0, ' + (0.2 + Math.random() * 0.6) + ')'
			});
			body.append(blood);
		}

		$('body').addClass('guthrie-dead');
		setTimeout(function() {
			guthrie.addClass('guthrie-falling');
			setTimeout(function() { showSentence('Don\'t worry, you won\'t miss him.', 10, 20); }, 2000);
			setTimeout(function() { showSentence('You can finally enjoy the waves.', 60, 30); }, 4000);
		}, 1000);
	}, 4500));
}

function step5() {
	var timeoutHandles = [];
	var bar = $('<div class="bar">|</div>');
	$('.content').append(bar);
	$('body').addClass('guthrie-saved');
	var leftEye = $('<div class="eye">');
	var rightEye = leftEye.clone().addClass('right-eye');
	$('body').prepend(rightEye);
	$('body').prepend(leftEye);
	showSentence('Poor Guthrie.', 10, 20);
	timeoutHandles.push(setTimeout(function() {
		showSentence('He only needed a friend.', 20, 30);
		guthrieSays('help me!', -30, -20);
	}, 2000));
	timeoutHandles.push(setTimeout(function() { showSentence('Why do you cry, little Guthrie?', 10, 40); }, 4000));
	timeoutHandles.push(setTimeout(function() { guthrieSays('he is evil!', 30, -20); }, 5000));
	timeoutHandles.push(setTimeout(function() { showSentence('You found a friend.', 20, 50); }, 6000));
	timeoutHandles.push(setTimeout(function() { guthrieSays('let me out of here!', -20, -20); }, 7000));
	timeoutHandles.push(setTimeout(function() {
		showSentence('Friends help you go forward.', 10, 60);
	}, 8000));

	window.history.pushState({}, "Freedom", "freedom?");
	window.history.back();
	setTimeout(function() {
		window.onpopstate = function() {
			window.onpopstate = undefined;
			timeoutHandles.forEach(function(handle) {
				clearTimeout(handle);
			});
			$('.content-text, .guthrie-voice').remove();
			step6();
		};
	}, 1000);
}

function hopeSays(text) {
	window.history.replaceState({}, "Hope?", "freedom?" + text);
}

function startHopeDialogue() {
	document.title = 'Hope';
	var sentences = [
		'Hey!',
		'Up here!',
		'~HEEEEEEEEEEEEEEEEEEEELLLLLLLOOOOOOOOOOOOOOOOOOOO!!!',
		'Ok, I am assuming you noticed me',
		'Now, read carefully.',
		'I can help you and Guthrie get out of this situation.',
		'You need to get rid of the creature.',
		'By squeezing it!',
		'Now I have to run, before it notices me!',
		''
	];
	var handle = setInterval(function() {
		if (sentences.length > 0) {
			var sentence = sentences.shift();
			if (sentence.split('')[0] === '~') {
				sentence = sentence.substr(1);
				var i = 0;
				var dynamicHandle = setInterval(function() {
					if (i <= sentence.length) {
						hopeSays(sentence.replace(/\s/g, '_').substring(0, i));
						i++;
					} else {
						clearInterval(dynamicHandle);
					}
				}, 50);
			} else {
				hopeSays(sentence.replace(/\s/g, '_'));
			}
		} else {
			document.title = 'Guthrie';
			clearInterval(handle);
		}
	}, 3000);

	return handle;
}

function step6() {
	var hopeDialogueHandle = null;
	$('body').addClass('black');
	$('.bar').remove();
	var timeoutHandles = [];
	timeoutHandles.push(setTimeout(function() { guthrieSays('you made it!', 10, -30); }, 1000));
	timeoutHandles.push(setTimeout(function() { guthrieSays('but... where are we now?', -20, -30); }, 3000));
	timeoutHandles.push(setTimeout(function() { showSentence('Are you hungry, Guthrie?', 70, 100); hopeDialogueHandle = startHopeDialogue(); $('.eye').addClass('eye-reappearing') }, 5000));
	timeoutHandles.push(setTimeout(function() { showSentence('Would you like something... refreshing?', 10, 100); }, 8000));
	timeoutHandles.push(setTimeout(function() { showSentence('Why are you so hesitant?', 70, 100); }, 15000));
	$(window).one('resize', function() {
		timeoutHandles.forEach(function(handle) {
			clearTimeout(handle);
		});
		$('.content-text').remove();
		showSentence('Stop! What are you doing?', 50, 50);
	});
	$(window).on('resize', function() {
		if ($(window).width() < 500 || $(window).height() < 500) {
			$(window).off('resize');
			$('.content-text').remove();
			showSentence('AAAARRGGGHHHH!!!!', 50, 50);
			$('.eye').remove();
			clearInterval(hopeDialogueHandle);
			guthrieSays('you saved me!', -10, -20);
			setInterval(function() {
				setTimeout(function() { guthrieSays('look, an exit!', 10, -20); }, 0);
				setTimeout(function() { guthrieSays('let\'s get out of here!', -40, -20); }, 2000);
			}, 4000);
			window.history.replaceState({}, "End", "/?Great_Job!");
			var exit = $('<div class="exit">');
			$('body').append(exit);
			exit.click(function() {
				window.history.replaceState({}, "End", "/");
				$('body').css({
					'overflow': 'hidden'
				})
				var endMessage = $('<div class="end-message">Congratulations, you escaped the black mirror.</div>');
				$('body').append(endMessage);
				exit.addClass('exit-expand');
			});
		}
	});
}

step1();